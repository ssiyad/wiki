---
title: "Software"
date: 2020-02-09T00:38:11+05:30
---
## PC
- [Arch Linux](https://www.archlinux.org/) as my OS
- [Firefox](https://www.mozilla.org/en-US/firefox/new/)
- [gimp](https://www.gimp.org/)
- [gparted](http://gparted.org/) and [parted](https://www.gnu.org/software/parted/) - Partitioning tool. There is also a live image.
- [kitty](https://github.com/kovidgoyal/kitty) as terminal emulator
- [nano](https://www.nano-editor.org/download.php) and [vim](https://www.vim.org/) - as text editors
- [PyCharm](https://www.jetbrains.com/pycharm/) as python IDE
- [ranger](https://github.com/ranger/ranger) for file management
- [telegram](https://desktop.telegram.org/)
- [VLC](https://www.videolan.org/vlc/)
- [VSCode](https://github.com/Microsoft/vscode)

## Android
- [AdAway](https://adaway.org/)
- [Arch packages](https://github.com/rascarlo/ArchPackages)
- [DriveDroid](https://www.drivedroid.io/)
- [Easer](https://github.com/renyuneyun/Easer)
- [F-Droid](https://f-droid.org/FDroid.apk)
- [FastHub-Libre](https://github.com/thermatk/FastHub-Libre)
- [Feeder](https://f-droid.org/en/packages/com.nononsenseapps.feeder/)
- [Firefox](https://www.mozilla.org/en-US/firefox/mobile/)
- [LibremTunnel](https://f-droid.org/en/packages/one.librem.tunnel)
- [LibreTasks](https://f-droid.org/en/packages/libretasks.app)
- [LibreTorrent](https://f-droid.org/en/packages/org.proninyaroslav.libretorrent)
- [Limbo](https://sourceforge.net/projects/limbopcemulator/)
- [Manglish](https://f-droid.org/en/packages/subins2000.manglish)
- [Manyverse](https://f-droid.org/en/packages/se.manyver)
- [Meshenger](https://f-droid.org/en/packages/d.d.meshenger)
- [MixPlorer](https://mixplorer.com/)
- [Moon+ Reader Pro](https://play.google.com/store/apps/details?id=com.flyersoft.moonreaderp&hl=en_US)
- [NewPipe](https://newpipe.schabi.org/)
- [OpenMultiMaps](https://f-droid.org/en/packages/app.fedilab.openmaps)
- [Plus Messenger](http://www.plusmessenger.org/)
- [Riot.im](https://f-droid.org/en/packages/im.vector.alpha)
- [Slide for Reddit](https://f-droid.org/en/packages/me.ccrama.redditslide)
- [SuperFreeze](https://f-droid.org/en/packages/superfreeze.tool.android)
- [Termux](https://f-droid.org/en/packages/com.termux)
- [Titanium Backup](https://play.google.com/store/apps/details?id=com.keramidas.TitaniumBackup&hl=en_US)
- [Tusky](https://f-droid.org/en/packages/com.keylesspalace.tusky)
- [Tutanota Mail](https://tutanota.com/)
- [Vinyl](https://f-droid.org/en/packages/com.poupa.vinylmusicplayer)
- [VLC](https://f-droid.org/en/packages/org.videolan.vlc)
