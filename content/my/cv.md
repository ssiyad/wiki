---
title: "CV"
date: 2020-01-26T12:54:09+05:30
---
# Sabu Siyad
#### Hello! I'm a professional backend developer equipped with modern programming languages.
Even though my preferred ﬁeld of work is in the backend, I can do frontend pretty much ﬂuently. The main reason why I choose backend is the creativity and opportunity that comes with it. A majority of the projects I completed (freelance and hobby) was on the backend. When doing frontend projects, I prefer ReactJS (JavaScript, Node), Django (Python), and Flask (Python) if the project is more focused on performance than on user experience.  

I have plenty of experience running multiple cloud servers for varying purposes, using DigitalOcean, AWS, and Google Cloud services. I always try to minimize downtime at critical times and thus have great conﬁdence in my abilities to maintain a server on my own.  

One of my greatest strengths is to learn and adopt new things. I ﬁnd myself very eﬃcient with reading and understanding manuals and other kinds of documentation. Another one is adaptability,
which is something I'm very proud of. It gives me the power to solve any problem thrown at me, whatever its nature is.  

My (hobby) projects are available at [GitLab](https://gitlab.com/ssiyad/).

### Education
I'm currently pursuing my bachelor's degree in computer science at Jawaharlal College of Engineering, Ottapalam (2017-2021) under Kerala Technical University.  

I've completed my higher secondary education (2013-2015) with 86% marks under the Kerala Board of Higher Secondary Examinations and my high school education in 2013 with 90% marks.

### Technical background
- Programming languages: *Python, Java, Rust, Go, C, C++, JavaScript, NodeJS, Lisp*
- Frameworks: *Django, Flask, ReactJS, AngularJS*
- Operating systems: *Arch, Ubuntu, Debian, Mint, Elementary, Slackware, Fedora, Parabola*
- Web-servers: *Apache, Nginx* 
- Automation: *Ansible*
- Database: *PostgreSQL, MongoDB, SQLite, Redis, MySQL*

### Hobbies
I like to read a lot, especially ﬁction and drama. Other hobbies include driving, football, and watching movies.

### Contact Details
- Address:
> Sabu Siyad  
> Pangattil, Alachully  
> Kottakkal, Malappuram, Kerala  
> PIN 676503
- Primary e-mail: [ssiyad@disroot.org](mailto:ssiyad@disroot.org)
- Secondary e-mail: [sabu.siyad.p@gmail.com](mailto:sabu.siyad.p@gmail.com)
- Telegram: [@ssiyad](https://t.me/ssiyad)
- Matrix: [@ssiyad:poddery.com](https://matrix.to/#/@ssiyad:poddery.com)

### Other
- IIP Head, Coders Club, Jawaharlal College of Engineering: 2019 - Present
- Languages:
  - Malayalam
  - English
  - Tamil
- Year of birth: 1998
