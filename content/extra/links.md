---
title: "Interesting Links"
date: 2020-02-09T02:04:20+05:30
---
- [Phishing with Unicode Domains](https://www.xudongz.com/blog/2017/idn-phishing/)
- [Troubleshooting in Linux](https://medium.com/@amitarlnct/troubleshooting-in-linux-2a199fb4977e)
- [A Beginner’s Guide to Surviving in the Linux Shell](https://medium.freecodecamp.org/a-beginners-guide-to-surviving-in-the-linux-shell-cda0f5a0698c)
- [I wrote a programming language. Here’s how you can, too.](https://www.freecodecamp.org/news/the-programming-language-pipeline-91d3f449c919/)
- [Why do Manjaro Stable updates break the system? Is it bad testing?](https://forum.manjaro.org/t/why-do-manjaro-stable-updates-break-the-system-is-it-bad-testing/21095)
- [Arch Discussion » Archlinux is moving to systemd](https://bbs.archlinux.org/viewtopic.php?pid=1149530#p1149530)
- [GoboLinux at a glance](https://gobolinux.org/at_a_glance.html)
- [Android phone - Linux Desktop integration](https://bbs.archlinux.org/viewtopic.php?id=231050)
- [50 Bytes of Code That Took 4 GB to Compile](https://randomascii.wordpress.com/2013/08/14/50-bytes-of-code-that-took-4-gb-to-compile/)
- [Patch Workflow With Mutt - 2019-Greg Kroah-Hartman](http://kroah.com/log/blog/2019/08/14/patch-workflow-with-mutt-2019/)
- [How to contribute (was Re: Kernelnewbies Digest, Vol 77, Issue 7](https://lists.kernelnewbies.org/pipermail/kernelnewbies/2017-April/017765.html)
- [Chrome implements DRM. So does Chromium, through nonfree software that is effectively part of it.](https://bugs.chromium.org/p/chromium/issues/detail?id=686430)
- [Nest Reminds Customers That Ownership Isn't What It Used to Be](https://www.eff.org/deeplinks/2016/04/nest-reminds-customers-ownership-isnt-what-it-used-be)

