---
title: "ADB push error in scrcpy"
date: 2020-11-19T11:54:00+05:30
tags: []
---
- [scrcpy](https://github.com/Genymobile/scrcpy) is a program used to display and control Android devices
- It uses `adb` to function
- Can also be used via `tcp` networks

### Issue
The *error#1* I faced was:
```
adb: error: stat failed when trying to push to /data/local/tmp/scrcpy-server.jar: Permission denied
ERROR: "adb push" returned with value 1
```
Due to
> `SELinux` denied shell permissons and
> `adb root` was not available

### Fix
1. Disable SELinux
    > I had to do this to circumevent *error#2*, need root (Magisk)
    ```shell
    adb shell
    su
    setenforce 0
    ```
2. Find relevant SELinux contexts *(Optional)*
    ```
    adb shell cat '/system/etc/selinux/*' | grep 'u:.*shell.*'
    ```
    - Result
    ```
    cat: /system/etc/selinux/mapping: Is a directory
    /(odm|vendor/odm)/bin/sh		u:object_r:vendor_shell_exec:s0
    /(vendor|system/vendor)/bin/sh		u:object_r:vendor_shell_exec:s0
    /mnt/expand/[^/]+/local/tmp(/.*)?		u:object_r:shell_data_file:s0
    /data/local/tmp(/.*)?		u:object_r:shell_data_file:s0
    /system/bin/sh		--	u:object_r:shell_exec:s0
    /system/xbin/bash		u:object_r:shell_exec:s0
    security.perf_harden    u:object_r:shell_prop:s0
    service.adb.root        u:object_r:shell_prop:s0
    service.adb.tcp.port    u:object_r:shell_prop:s0
    ```
3. Rename the SELinux context from system_data_file to shell_data_file
    > From [here](https://stackoverflow.com/questions/59046671/android-shell-user-has-no-access-to-data-local-tmp-even-though-it-is-the-owner)
    ```
    adb shell
    su
    chcon -R u:object_r:shell_data_file:s0 /data/local/tmp
    ```
    - Possible *error#2*
    ```
    chcon: '/data/local/tmp' to u:object_r:shell_prop:s0: Permission denied
    ```
