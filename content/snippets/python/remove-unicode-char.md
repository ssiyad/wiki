---
title: "Remove Unicode Characters from string (Regex)"
date: 2020-03-12T23:00:02+05:30
---
```python
import re

def clean_text(text):
    return re.sub(r"[^\x00-\x7F]*", "", text).strip()
```
