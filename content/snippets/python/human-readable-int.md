---
title: "Human Readable Int"
date: 2020-02-09T01:49:13+05:30
---
```python
import math

millnames = ['', ' K', ' M', ' B', ' T']

def human_readable_int(number):
    # https://stackoverflow.com/questions/3154460/python-human-readable-large-numbers
    n = float(number)
    hri = max(0, min(len(millnames)-1,
                     int(math.floor(0 if n == 0 else math.log10(abs(n))/3))))

    return '{:.0f}{}'.format(n / 10**(3 * hri), millnames[hri])
```
