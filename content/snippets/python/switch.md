---
title: "Switch"
date: 2020-11-02T14:55:34+05:30
tags: []
---

Efficient illustration of (non-existing) Switches in python
```
def switch(choice: str):
    return {
        "choice1": "foo",
        "choice2": "bar",
    }[choice]
```
Dictionary values are evaluated before selection so [NameError](https://docs.python.org/3/library/exceptions.html#NameError) and other exceptions may occur.
