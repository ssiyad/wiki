---
title: "Hugo Section Tree"
date: 2020-02-09T01:21:29+05:30
---
`layouts/partials/sect.html`
```html
{{ if .File.Dir }}
    {{ if .IsPage }}
        <li>
            <a href="{{ .RelPermalink }}">{{ .Title }}</a>
        </li>
    {{ else }}
        <li>
            {{ .Title }}
        </li>
        <ul>
            {{ range .Pages.ByTitle }}
                {{ partial "tree.html" . }}
            {{ end}}
        </ul>
    {{ end }}
{{ end }}
```
