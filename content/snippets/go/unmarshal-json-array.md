---
title: "Unmarshal Json Array"
date: 2020-04-03T00:45:15+05:30
---
```
func (d *Data) UnmarshalJSON(buf []byte) error {
	tmp := []interface{}{&d.One, &d.Two, &d.Three, &d.Four}
	wantLen := len(tmp)
	if err := json.Unmarshal(buf, &tmp); err != nil {
		return err
	}
	if g, e := len(tmp), wantLen; g != e {
		return fmt.Errorf("wrong number of fields in Notification: %d != %d", g, e)
	}
	return nil
}
```
